#!/usr/bin/env bash

# Update target references with proper link

BASE_URL="http://apc-dev.web.cern.ch/docs"

function die() {
    echo "Error: $1" >&2
    exit 1
}

SCRIPT_DIR=$(cd $(dirname $0); pwd)
cd "$SCRIPT_DIR/.."

for f in references/*; do
    TARGET=$(grep '<reference' "$f" | sed -e 's#.*target=['\''"]\([^'\''"]*\).*#\1#')
    SOURCE=$(echo $TARGET|sed -e 's#\.\(html\|txt\|pdf\)$##')

    if [ -e "${SOURCE}.pug" ]; then
        pug -E xml "${SOURCE}.pug" || die "Failed to generate XML file for $f"
    fi
    SOURCE="${SOURCE}.xml"
    if [ -e "${SOURCE}" ]; then
        DEST=$(${SCRIPT_DIR}/get_dist_dir.sh "$SOURCE")
        echo "Updating reference file: ${f}"
        URL="${BASE_URL}/${DEST}/${TARGET}"
        sed -i "$f" -e 's#\(<reference.*target=\)['\''"][^'\''"]*['\''"]#\1"'${URL}'"#'
    fi
done
