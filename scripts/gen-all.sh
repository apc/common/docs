#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname $0); pwd)

cd "$SCRIPT_DIR/.."

function die() {
    echo "Error: $1" >&2
    exit 1
}

set -e

echo "Cleaning intermediate files"
for f in *.pug; do
    f=$(echo $f | sed 's#.pug#.xml#')
    rm -f "$f"
done

echo "Cleaning generated files"
rm -f *.pdf *.html *.txt

echo "Generating XML docs"
for f in *.xml; do
    [ -e "$f" ] || continue
    xml2rfc -c .cache "$f" || die "Failed to generate document: $f"
done

echo "Generating PUG docs"
for f in *.pug; do
    [ -e "$f" ] || continue
    xml2rfc -c .cache "$f" || die "Failed to generate document: $f"
done

echo "done"
