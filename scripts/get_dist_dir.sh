#!/usr/bin/env bash

xml2rfc --is-draft "$1" >/dev/null 2>&1
if [ $? -eq 0 ]; then
    echo "drafts/current"
else
    CATEGORY=$(xml2rfc "$1" --get-category)
    if [ -z "$CATEGORY" ]; then
        echo "error"
        exit 1
    else
        echo "${CATEGORY}"
    fi
fi
