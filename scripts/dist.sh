#!/usr/bin/env bash

SCRIPT_DIR=$(cd $(dirname $0); pwd)

cd "$SCRIPT_DIR/.."

function die() {
    echo "Error: $1" >&2
    exit 1
}

function dist() {
    NAME=$(echo $1 | sed 's#.xml##')
    DEST="$2"

    cp "${NAME}".{pdf,html,txt} "dist/$DEST/"
    if [ -e "$NAME" ]; then
        cp -ra "$NAME" "dist/${DEST}"
    fi
}

function dist_pub() {
    ID=$(xml2rfc "$1" --get-number)
    DEST="$2"
    NAME=$(echo $1 | sed 's#.xml##')

    for ftype in pdf html txt; do
        cp "${NAME}.${ftype}" "dist/${DEST}/by-id/${ID}.${ftype}"
    done
}

echo "Cleaning dist directory"
rm -rf dist
mkdir -p dist/drafts/current dist/{std,bcp,exp,info,historic}/by-id
# link current and expired, we don't use this in the process
ln -s current dist/drafts/expired

echo "Preparing dist directory"
for f in *.xml; do
    [ -e "$f" ] || continue

    DEST=$(${SCRIPT_DIR}/get_dist_dir.sh "$f")
    if [ "$DEST" = "error" ]; then
        echo "Failed to dist $f"
        continue
    fi
    xml2rfc --is-draft "$f"
    dist "$f" "$DEST"
    if [ $? -ne 0 ]; then
        dist_pub "$f" "$DEST"
    fi
done

echo "Copying references"
mkdir -p dist/references
cp references/* dist/references

echo "Generating json index"
xml2rfc-index . -o index.json || die 'Failed to generate json index'
mv index.json dist

echo "Generating site"
pug scripts/index.pug || die 'Failed to generate site'
mv scripts/index.html dist
